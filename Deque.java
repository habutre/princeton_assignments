import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private Item[] queue;
    private int first = 0;
    private int last;
    
    @SuppressWarnings("unchecked")
    public Deque() { // construct an empty deque
        queue = (Item[]) new Object[0];
        last = 0;
    }

    public boolean isEmpty() { // is the deque empty?
        return this.size() == 0;
    }

    public int size() { // return the number of items on the deque
        int size = 0;
        
        for (Item i: this.queue) {
         if (null != i) {
                size++;
            }
        }
        
        return size;
    }

    public void addFirst(Item item) { // add the item to the front
        if (null == item)
            throw new NullPointerException();
        
        if (this.queue.length > this.size()) {
        	for (int i = this.size(); i >=1; i--) {
        		this.queue[i] = this.queue[i-1];
        	}
        	this.queue[0] = item;
        } else {
        
        if (first > 0) {
            this.queue[first-1] = item;
            first = first -1;
        } else {
            this.queue = this.growOnBegin(item, this.queue);
        }
        }
      }

    public void addLast(Item item) { // add the item to the end
        if (null == item)
            throw new NullPointerException();
        
        if (last < this.queue.length -1) {
            this.queue[last+1] = item;
            last = last+1;
        } else {
            this.queue = this.growOnEnd(item, this.queue);
        }
    }

    public Item removeFirst() { // remove and return the item from the front
        if (this.isEmpty())
            throw new NoSuchElementException();
        
        Item it = this.queue[first];
        
        this.queue[first] = null;
        first = first + 1;
        
        return it;
    }

    public Item removeLast() { // remove and return the item from the end
        if (this.isEmpty())
            throw new NoSuchElementException();
        

        Item it = this.queue[last];
        
        this.queue[last] = null;
        last = last - 1;
        
        return it;
    }

    @Override                          // return an iterator over items in order
    public Iterator<Item> iterator() { // from front to end
        Iterator<Item> it = new Iterator<Item>() {
            private int curIndex = 0;
            
            @Override
            public boolean hasNext() {
                return curIndex < last;
            }

            @Override
            public Item next() {
                if (last == queue.length)
                    throw new NoSuchElementException();
                
                return queue[curIndex++];
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        
        return it;
    }
    
    @SuppressWarnings("unchecked")
    private Item[] growOnBegin(Item it, Item[] curQueue) {
        // maybe due performance requirements the size should be 1.5
    	Item[] copy = null;
    	
    	if (curQueue.length > 0){
    		copy = (Item[]) new Object[curQueue.length*2];
        
    		int firstIndex = copy.length - (curQueue.length);
    		copy[firstIndex] = it;
        
	        for (int i = 1; i < curQueue.length; i++) {
	            copy[firstIndex+i] = curQueue[i];
	        }
	        
	        this.first = firstIndex;
    		this.last = this.size();
	        
    	} else {
	        copy = (Item[]) new Object[2];
    		copy[0] = it;
    		this.first = 0;
    		this.last = 0;
    	}
        
        return copy;
    }

    @SuppressWarnings("unchecked")
    private Item[] growOnEnd(Item it, Item[] curQueue) {
    	Item[] copy = null;
    	
    	if (curQueue.length > 0) { 
	    	copy = (Item[]) new Object[curQueue.length*2];
	        
	        int lastIndex = curQueue.length + 1;
	        int idx = lastIndex-1;
	        
	        for (Item i : curQueue) {
	            copy[idx--] = i;
	        }
	        
	        copy[lastIndex] = it;
	        this.last = lastIndex;
	        this.first = copy.length - (this.size()+1);
	        
    	} else {
    		copy = (Item[]) new Object[2];
    		copy[1] = it;
    		this.first = 1;
    		this.last = 1;
    	}
        
        return copy;
    }
    
    public static void main(String[] args) { // unit testing
        
    }

}