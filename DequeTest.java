import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DequeTest {

	private Deque<Integer> deque;
	
	@Before
	public void setUp() throws Exception {
		deque = new Deque<Integer>();

		for(int i = 1; i <= 5; i++) {
			if (i % 2 == 0) {
				deque.addFirst(i);
			} else {
				deque.addLast(i);
			}
		}
	}

	@After
	public void tearDown() throws Exception {
		deque = null;
	}

	@Test
	public void test01IsNotEmpty() {
		Assert.assertFalse(deque.isEmpty());
	}
	
	@Test
	public void test02IsEmpty() {
		deque = new Deque<Integer>();
		Assert.assertTrue(deque.isEmpty());
	}

	@Test
	public void test03Size() {
		Assert.assertEquals(5, deque.size());
	}

	@Test
	public void test04AddFirst() {
		deque.addFirst(1);
		Assert.assertEquals(6, deque.size());
	}
	
	@Test(expected=NullPointerException.class)
	public void test05AddFirstThrowsException() {
		deque.addFirst(null);
	}
	
	@Test
	public void test06AddLast() {
		deque.addLast(2);
		Assert.assertEquals(6, deque.size());
	}
	
	@Test(expected=NullPointerException.class)
	public void test07AddLastThrowsException() {
		deque.addLast(null);
	}
	
	@Test
	public void test08RemoveFirst() {
		Integer it = deque.removeFirst();
		Assert.assertEquals(1, it.intValue());
	}

	@Test
	public void test09RemoveLast() {
		fail("Not yet implemented");
	}

	@Test
	public void test10Iterator() {
		fail("Not yet implemented");
	}

}
