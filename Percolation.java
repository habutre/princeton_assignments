import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.Stopwatch;

public class Percolation {

    private static final double TRESHOULD = 0.51f;
    private int[][] grid;
    private int dimension;

    public Percolation(int n) {
        // create n-by-n grid, with all sites blocked
        if (n <= 0)
            throw new IllegalArgumentException();

        this.dimension = n;
        this.grid = this.init(this.dimension);
    }

    private int[][] init(int dim) {
        int[][] _grid = new int[dim][dim];

        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++)
                _grid[i][j] = 1;
		}

		return _grid;
	}

	public void open(int i, int j) {
		// open site (row i, column j) if it is not open already
		int[] indexes = new int[2];

		boolean check = this.isFull(i, j);

		if (check) {
			this.grid[i][j] = 0;
			indexes[0] = i;
			indexes[1] = j;
		}

	}

	public boolean isOpen(int i, int j) {
		// is site (row i, column j) open?
		return this.grid[i][j] == 0;	
	}

	public boolean isFull(int i, int j) {
		// is site (row i, column j) full?
		return this.grid[i][j] == 1;
	}

	private int totalOpened() {
		int total = 0;

		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				if (this.isOpen(i, j)) {
					total++;
				}
			}
		}

		return total;
	}

	public boolean percolates() {
		// does the system percolate?
		int total = dimension * dimension;
		int opened = this.totalOpened();

		double reason = (double) opened / total;

		//System.out.println("Percolates?: " + opened + " and " + total + " = "
			//+ reason + " Percolates? " + (reason > TRESHOULD));

		return reason >= TRESHOULD;
	}

	public static void main(String[] args) {
		// test client (optional)

		int n = 0;

		if (args.length > 0)
			n = Integer.parseInt(args[0]);

		n = 500;

		Stopwatch watch = new Stopwatch();

		Percolation p = new Percolation(n);

		while (!p.percolates()) {
			int i = StdRandom.uniform(n);
			int j = StdRandom.uniform(n);
			p.open(i, j);
		}

		System.out.println("Got the percolation with: " + 
				p.totalOpened() + " in " + watch.elapsedTime());
	}
}
