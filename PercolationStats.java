import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

	private int n;
	private int trials;

	public PercolationStats(int n, int trials) {    
		boolean violates = n <= 0 || trials <= 0;

		if (violates)
			throw new IllegalArgumentException();

		this.n = n;
		this.trials = trials;
	}

	public double mean() {
		// sample mean of percolation threshold
		double result = StdStats.mean(new double [0]);
		
		return result;
	}

	public double stddev() {
		// sample standard deviation of percolation threshold
		double result = StdStats.stddev(new double[0]);
		
		return result;
	}

	public double confidenceLo() {
		// low  endpoint of 95% confidence interval
		return 0d;
	}

	public double confidenceHi() {
		// high endpoint of 95% confidence interval
		return 0d;
	}

	public static void main(String[] args) {
		// test client (described below)
		if (args.length < 2)
			throw new IllegalArgumentException();
		
		int n = Integer.parseInt(args[0]);
		int trials = Integer.parseInt(args[1]);
		
	}
}
