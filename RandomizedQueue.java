import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private Item[] queue;
    
    @SuppressWarnings("unchecked")
    public RandomizedQueue() {                           // construct an empty deque
        this.queue = (Item[]) new Object[10];
    }

    public boolean isEmpty() { // is the deque empty?
        return false;
    }

    public int size() { // return the number of items on the deque
        return this.queue.length;
    }

    public void enqueue(Item item) { // add the item
        
    }
    
    public Item dequeue() { // remove and return a random item
        return null;
    }

    public Item sample() {
        return null;
    }

    @Override                          // return an iterator over items in order
    public Iterator<Item> iterator() { // from front to end
        Iterator<Item> it = new Iterator<Item>() {
        	int curIndex = 0;

            @Override
            public boolean hasNext() {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public Item next() {
            	if (curIndex == queue.length)
            		throw new NoSuchElementException();

                return null;
            }
            
            @Override
            public void remove() {
                
            }
        };
        
        return it;
    }

    public static void main(String[] args) { // unit testing
    }
}